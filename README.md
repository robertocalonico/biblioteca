# Library

Book management

## Starting

_These instructions will allow you to obtain a copy of the running project on your local machine for development and testing purposes._


### Pre-requirements 📋

```
Install GIT
Instsall Composer
```

### Installation 🔧

Using your console, perform an installation using composer

```
composer install
```

_And_

```
npm install
```
Create a database 
```
create database library;
```
Make the connection Laravel with database (copy the file .env.example) and rename to .env

_Generate a key with_
```
php artisan key:generate
```
Run migrations of database tables
```
php artisan migrate
```