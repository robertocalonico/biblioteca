@extends('layouts.app')
@section('title', 'Books')
@section('content')
    <add-category-component></add-category-component>
    <add-book-component></add-book-component>
    <books-component></books-component>
    <creat-category-component></creat-category-component>
    <creat-book-component></creat-book-component>
@endsection