<?php

namespace Library\Http\Controllers;
use Library\Book;
use Illuminate\Http\Request;
use Library\Category;
use Illuminate\Support\Facades\DB;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category, Request $request)
    {
        $books = DB::table('books')
            ->join('categories', 'books.category_id', '=', 'categories.id')
            ->select('books.*', 'categories.name as category')
            ->get();
        if($request->ajax()){            
            return response()->json($books, 200);
    	}
        return view('books', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            $book = new Book();
            $book->name = $request->input('name');
            $book->author = $request->input('author');
            $book->category_id = $request->input('category');
            $book->published_date = $request->input('published_date');
            $book->user = $request->input('user') !== null ? $request->input('user') : '';
            $book->save();

            return response()->json([
                "message" => "Book added successfully",
                "book" => $book
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->ajax()){
            $book = Book::find($request->input('id'));
            if($request->input('available') !== null) {
                if($request->input('available') == 'available'){
                    $book->user = '';
                } else {
                    $book->user = $request->input('user');
                }
            } else {
                $book->name = $request->input('name');
                $book->author = $request->input('author');
                $book->category_id = $request->input('category');
                $book->published_date = $request->input('published_date');
            }
            $book->save();

            return response()->json([
                "message" => "Book updated successfully",
                "book" => $book
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $book = Book::find($id);
        $book->delete();
        return response()->json([
            "message" => "Book is deleted",
            "book" => $book
        ], 200);
    }
}
