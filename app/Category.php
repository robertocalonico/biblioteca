<?php

namespace Library;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function books()
    {
        return $this->hasMany('Library\Book');
    }
}