<?php

namespace Library;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function categories()
    {
        return $this->belongsTo('Library\Category');
    }
}
